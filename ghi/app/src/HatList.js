import { useEffect, useState } from "react";

function HatList() {
  const [hats, setHats] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/hats/");

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Style</th>
            <th>Fabric</th>
            {/* <th>Location</th> */}
            <th>Color</th>
            <th>Location ID</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {hats.map((hat) => {
            return (
              <tr key={hat.id}>
                <td>{hat.style_name}</td>
                <td>{hat.fabric}</td>
                {/* <td>{hat.location.closet_name}</td> */}
                <td>{hat.color}</td>
                <td>{hat.location.id}</td>
                <td>
                  <img width="200" length="200" src={hat.picture_url} />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <button className="btn btn-primary"> Add hat</button>
    </>
  );
}

export default HatList;
