import React, { useEffect, useState } from "react";

function HatForm() {
  const [locations, setLocations] = useState([]);
  const [formData, setFormdata] = useState({
    style: "",
    fabric: "",
    color: "",
    location: "",
    picture_url: "",
  });

  const getData = async () => {
    const url = "http://localhost:8100/api/locations/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const locationUrl = "http://localhost:8090/api/hats/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const resposnse = await fetch(locationUrl, fetchConfig);
    if (resposnse.ok) {
      setFormdata({
        style: "",
        fabric: "",
        color: "",
        location: "",
        picture_url: "",
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormdata({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.style}
                placeholder="Style"
                required
                type="text"
                name="style"
                id="style"
                className="form-control"
              />
              <label htmlFor="style">Style</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.fabric}
                placeholder="Fabric"
                required
                type="text"
                name="fabric"
                id="fabric"
                className="form-control"
              />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.color}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.picture_url}
                placeholder="Picture"
                required
                type="text"
                name="picture"
                id="picture"
                className="form-control"
              />
              <label htmlFor="picture">Picture</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleFormChange}
                value={formData.location}
                required
                name="location"
                id="location"
                className="form-select"
              >
                <option value="">Choose a location</option>
                {locations.map((location) => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatForm;
