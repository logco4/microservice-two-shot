import { useEffect, useState } from "react";


function ShoeList() {
    const [shoes, setShoes] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8080/api/shoes/");

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <>
        <table className="table table-striped">
            <thead>
                <tr>
                    <td></td>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Color</th>
                    <th>Bin ID</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td>
                                <img width="200" src={shoe.picture_url} />
                            </td>
                            <td>{shoe.name}</td>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.bin.id}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        <button className="btn btn-primary">Add shoe +</button>
        </>
    );
}

export default ShoeList;
