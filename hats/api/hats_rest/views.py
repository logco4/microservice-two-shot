from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import LocationVO, Hat

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name", "id"]

class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
       "fabric",
       "style_name",
       "id",
       "color",
       "picture_url",
       "location",
    ]

    encoders = {
        "location": LocationVOEncoder(),
    }
    # def get_extra_data(self, o):
    #     return {"location": o.location.import_href}

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder = HatEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid Location Id"},
            status = 400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder = HatEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "PUT", "GET"])
def api_hat(request, id):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=id)
            return JsonResponse(
                hat,
                encoder = HatEncoder,
                safe = False,
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = LocationVO.objects.get(import_href=content["location"])
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status = 400,
            )
        Hat.objects.filter(id=id).update(**content)
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder = HatEncoder,
            safe = False
        )
